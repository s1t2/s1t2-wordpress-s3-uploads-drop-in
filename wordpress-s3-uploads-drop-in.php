<?php
/*
Plugin Name: S3 Uploads DropIn
Version: 2.3.3
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use Aws\S3\S3Client;
use Aws\Exception\AwsException;
use Aws\S3\ObjectUploader;

add_action( 'plugins_loaded', array( S3Uploads::get_instance(), 'init' ) );

// A little singleton
class S3Uploads {

	protected function __construct() { }
	protected function __wakeup() { }
	protected function __clone() { }

	public static function get_instance() {
		static $instance = null;
		if ( $instance === null ) {
			$instance = new static();
		}
		return $instance;
	}

	private $config;

	private $s3Client;

	public function init() {
		$this->initS3Settings();

		// media library
		add_filter( 'upload_dir', array( $this, 'filter_upload_dir' ) );
		add_filter( 'wp_unique_filename', array( $this, 'wp_unique_filename' ), 10, 3 );
		add_filter( 'wp_generate_attachment_metadata', array( $this, 'on_updated_attachment' ), 10, 2 );
		add_action( 'delete_attachment', array( $this, 'on_before_deleted' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'load_custom_styles' ), 99 );

		// bb-plugin
		add_action( 'fl_builder_after_save_layout', array( $this, 'upload_builder_caches' ), 10, 1 );
		add_action( 'fl_builder_after_save_draft', array( $this, 'upload_builder_caches' ), 10, 1 );
		add_action( 'render_node', array( $this, 'upload_builder_caches' ) );
		add_action( 'save_post', array( $this, 'upload_builder_caches' ), 10, 1 );
	}

	private function initS3Settings() {
		$config = array(
			'region'  => $_ENV['AWS_S3_REGION'],
			'version' => 'latest',
		);

		if ( isset( $_ENV['AWS_ACCESS_KEY_ID'] ) && isset( $_ENV['AWS_SECRET_ACCESS_KEY'] ) ) {
			$config['credentials'] = array(
				'key'    => $_ENV['AWS_ACCESS_KEY_ID'],
				'secret' => $_ENV['AWS_SECRET_ACCESS_KEY'],
			);
		}
		$this->config = $config;
	}

	public function initS3Client() {
		$this->s3Client = S3Client::factory( $this->config );
	}

	public function filter_upload_dir( $dirs ) {
		$dirs['url']     = $_ENV['AWS_S3_BUCKET_URL'] . $dirs['subdir'];
		$dirs['baseurl'] = $_ENV['AWS_S3_BUCKET_URL'];
		return $dirs;
	}

	// Replicate WordPress unique filenames for objects stored in S3
	public function wp_unique_filename( $filename, $ext, $dir ) {
		if ( ! $this->s3Client ) {
			$this->initS3Client();
		}

		$name   = basename( $filename, $ext );
		$bucket = $_ENV['AWS_S3_BUCKET'];
		$number = 1;

		// Replace the local dir with the s3 path
		$uploads_dir = wp_get_upload_dir();
		$base_dir    = $uploads_dir['basedir'];
		$s3_dir      = str_replace( $base_dir, $_ENV['AWS_S3_PATH'], $dir );

		$new_name = $name;
		while ( true ) {
			$exists = $this->s3Client->doesObjectExistV2( $bucket, $s3_dir . '/' . $new_name . $ext );
			if ( $exists ) {
				$new_name = $name . '-' . $number;
				$number++;
			} else {
				break;
			}
		}
		$new_filename = $new_name . $ext;
		return $new_filename;
	}

	public function on_updated_attachment( $attachmentData, $attachmentId ) {
		$attachmentData['filesize'] = filesize( get_attached_file( $attachmentId ) );

		$attachments = array_unique(
			array_values(
				array_merge(
					array( $this->attachmentMainPath( $attachmentId ) ),
					$this->attachmentOtherPaths( $attachmentId )
				)
			)
		);

		foreach ( $attachments as $attachment ) {
			$this->uploadToS3( $attachment );
			@unlink( $attachment );
		}

		return $attachmentData;
	}

	public function on_before_deleted( $attachmentId ) {
		if ( ! $this->s3Client ) {
			$this->initS3Client();
		}
		$attachments = array_map(
			function( $attachment ) {
				$seperators = explode( 'uploads', $attachment );
				return $_ENV['AWS_S3_PATH'] . end( $seperators );
			},
			array_merge(
				array( $this->attachmentMainPath( $attachmentId ) ),
				$this->attachmentOtherPaths( $attachmentId )
			)
		);

		$this->s3Client->deleteObjects(
			array(
				'Bucket' => $_ENV['AWS_S3_BUCKET'],
				'Delete' => array(
					'Objects' => array_map(
						function( $key ) {
								return array( 'Key' => $key );
						},
						$attachments
					),
				),
			)
		);
	}

	public function uploadToS3( $path ) {
		if ( ! $this->s3Client ) {
			$this->initS3Client();
		}
		$source     = fopen( $path, 'rb' );
		$path_parts = explode( 'uploads', $path );
		$key        = $_ENV['AWS_S3_PATH'] . end( $path_parts );
		$uploader   = new ObjectUploader(
			$this->s3Client,
			$_ENV['AWS_S3_BUCKET'],
			$key,
			$source,
			'private',
			array(
				'params' => array(
					'CacheControl' => 'max-age=15552000',
				),
			)
		);
		$uploader->upload();
	}

	private function attachmentLocation( $attachmentId ) {
		return pathinfo( $this->attachmentMainPath( $attachmentId ) )['dirname'];
	}

	private function attachmentMainPath( $attachmentId ) {
		return get_attached_file( $attachmentId );
	}

	private function attachmentOtherPaths( $attachmentId ) {
		return array_filter(
			array_map(
				function( $size ) use ( $attachmentId ) {
						$sizeInfo           = image_get_intermediate_size( $attachmentId, $size );
						$attachmentLocation = $this->attachmentLocation( $attachmentId );
						return isset( $sizeInfo['file'] ) ? $attachmentLocation . '/' . $sizeInfo['file'] : null;
				},
				$this->sizes( $attachmentId )
			)
		);
	}

	private function attachmentPath( $sizeInfo ) {
		return isset( $sizeInfo['file'] ) ? wp_get_upload_dir()['path'] . '/' . $sizeInfo['file'] : null;
	}

	private function sizes( $attachmentId ) {
		return array_keys( wp_get_attachment_metadata( $attachmentId )['sizes'] ?? array() );
	}

	public function upload_builder_caches( $post_id ) {
		if ( ! is_plugin_active( 'bb-plugin/fl-builder.php' ) ) {
			return;
		}
		if ( empty( $post_id ) ) {
			return;
		}
		$cache_dir   = 'bb-plugin/cache';
		$path        = wp_upload_dir()['basedir'] . "/$cache_dir/";
		$prefix      = $post_id . '-layout';
		$suffix      = array(
			'.css',
			'.js',
			'-partial.css',
			'-partial.js',
			'-draft.css',
			'-draft.js',
			'-draft-partial.css',
			'-draft-partial.js',
			'-preview.css',
		);
		$cache_files = array_map(
			function ( $suffix ) use ( $path, $prefix ) {
				$cache_file = $path . $prefix . $suffix;
				// create preview from draft
				if ( ! file_exists( $cache_file ) && $suffix == 'preview.css' && file_exists( $path . $prefix . '-draft.css' ) ) {
					copy( $path . $prefix . '-draft.css', $cache_file );
				}
				return $cache_file;
			},
			$suffix
		);
		foreach ( $cache_files as $cache_file ) {
			if ( file_exists( $cache_file ) ) {
				// upload to s3
				$this->uploadToS3( $cache_file );
			}
		}
	}

	public function load_custom_styles( $page ) {
		?>
<style>
	.media-modal .attachment-actions {
		display: none;
	}
</style>
		<?php
	}
}
